<?php

/* 
 * Data base generator for site test
 * 
 */
echo 'Data base generation started ';
$newPostSummary = array();
$newPostBody = array();
$summary = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy '
		. 'eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam '
		. 'voluptua. At vero eos et accusam et justo duo dolores et ea rebum.';
$body = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy '
		. 'eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam '
		. 'voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet '
		. 'clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit '
		. 'amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam '
		. 'nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, '
		. 'sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. '
		. 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor '
		. 'sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed '
		. 'diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, '
		. 'sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. '
		. 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

for($i = 0; $i < 100; $i++){
	$newPostSummary['id'] = $newPostBody['id'] = sha1(time() . mt_rand(0, 10000));
	$newPostSummary['title'] = $newPostBody['title'] = 'Post # ' . $i;
	$newPostSummary['summary'] = $newPostBody['summary'] = $summary;
	$newPostSummary['date'] = $newPostBody['date'] = date('d.m.Y H:i:s');
	$newPostBody['body'] = $body;
	$fileNameID = $newPostBody["id"];
	
	$posts = $posts . "\n" . json_encode($newPostSummary); 
	file_put_contents("db/$fileNameID.json", json_encode($newPostBody));
	if($i % 10 == 0){
		echo '.';
	}
}

file_put_contents("db/post.json", $posts);

echo ' Data base generated';