<?php
/* 
 * Получение записей со стены во vKontakte 
 */

$config = require_once ('config.php');
require_once ('class-db.php');

$fileDb = new FileDb('db/', 'r', $config['pageSize']);

require_once ('function.php');
$currentMode = 'importWall';	// переменная для подсветки текущего пункта меню (menu.php)
$curHeadTitle = 'VK wall import';	// переменная для вывода title в head блога (menu.php)

if (isset($_SESSION['access_token'])){
	$url = "https://api.vk.com/method/wall.get" ;
	$postfields = [
		'owner_id'		 => '3954721',
		'count'			 => '20',
		'offset'		 => '0',
		'filter'		 => 'owner',
		'access_token'	 => $_SESSION['access_token'],
	];

	if ($data = curl_query($url,$postfields)){
		$data = json_decode($data, TRUE);
		unset($data['response'][0]);

		if (!empty($_POST)){
			$keys = array_keys($_POST);
			foreach ($data['response'] as $value) {
				if (in_array($value['id'], $keys)){
					$title		= $value['id'];
					$summary	= substr($value['text'], 0, 200);
					$body	= $value['text'];
					$fileDb->createPost($title, $summary, $body);
				}
			}
			header('location: index.php');	//переход к просмотру постов
		}

		require_once ('tpl/importwall.php');
	}
}