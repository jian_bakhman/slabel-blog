<?php
/* 
 *  login.php in main blog
 */
$config = require_once ('config.php');
$currentMode = 'login';		// переменная для подсветки текущего пункта меню (menu.php)
$curHeadTitle = 'Login page';	// переменная для вывода title в head блога (menu.php)

if (isset($_SESSION['auth'])){
    header('Location: index.php');
}
/* Авторизация на сайте */
if (isset($_POST['password'])){
    if ($_POST['username'] == $config['username'] && sha1($_POST['password']) == $config['password']){
        $_SESSION['auth'] = TRUE;
        if (!isset($_SESSION['loginForCreate'])){
			header('Location: index.php');
		} else {
			unset($_SESSION['loginForCreate']);
			header('Location: create.php');
		}
    } else {
        $error = TRUE;
    }
}

require_once ('tpl/login.php');



