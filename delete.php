<?php
/* 
 *  Delete post from blog
 */
$config = require_once ('config.php');
require_once ('class-db.php');

$fileDb = new FileDb('db/', 'r', $config['pageSize']);

$id = $_GET['id'];	// получение из &_GET id удаляемого поста

$fileDb->deletePost($id);

header('Location: index.php');