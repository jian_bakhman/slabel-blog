<?php
/* 
 *  logout.php in main blog
 */
$config = require_once ('config.php');

if (isset($_SESSION['auth'])){
	unset($_SESSION['auth']);
	unset($_SESSION['authType']);
}
header('Location: index.php');



