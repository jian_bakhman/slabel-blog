<?php
/* 
 * Modify post
 * 
 */
$config = require_once ('config.php');
require_once ('class-db.php');

$fileDb = new FileDb('db/', 'r', $config['pageSize']);

if (isset($_GET['id'])){
	$id = $_GET['id'];
	$filename = "$id.json";  //получаем имя файла поста
	if (file_exists("db/$filename")){
		if (isset($_POST['title']) && isset($_POST['summary']) && isset($_POST['body'])){

			$fileDb->updatePost($id, $_POST['title'], $_POST['summary'], $_POST['body']);

			header ("Location: post.php?id=$id");
			
		} else {
			$post = json_decode(file_get_contents("db/$filename"), TRUE);
			$curHeadTitle = $post['title'];		// переменная для вывода title в head блога (menu.php)
			require_once ('tpl/modify.php');
		}
	}
}
