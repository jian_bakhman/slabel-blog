<?php
/* 
 *  Template main menu
 */
?>
	<ul class="nav nav-pills nav-stacked">
		<li <?php echo ($currentMode == 'index') ? 'class="active"' : '' ?>>
			<a href="index.php">Main</a>
		</li>
		<li <?php echo ($currentMode == 'create' || $currentMode == 'loginForCreate') ? 'class="active"' : '' ?>>
			<a href="create.php">Create Post</a>
		</li>
		<?php
		if (isset($_SESSION['auth']) && $_SESSION['authType'] == 'vk'){ ?>
		<li <?php echo ($currentMode == 'importWall') ? 'class="active"' : '' ?> >
			<a href="importWall.php">Import VK wall</a>
		</li>
		<?php } ?>
		<?php
		if (!isset($_SESSION['auth'])){ ?>
		<li <?php echo ($currentMode == 'login') ? 'class="active"' : '' ?> >
			<a href="login.php">Login</a>
		</li>
		<?php } ?>
		<?php
		if (isset($_SESSION['auth'])){ ?>
		<li>
			<a href="logout.php">Logout</a>
		</li>
		<?php } ?>
	</ul>