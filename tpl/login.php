	<?php
	require_once 'header.php';
	?>
    <div class="container-fluid">
        <div class="col-md-3">
			<?php
			require_once 'menu.php';
			?>
        </div>
        <div class="col-md-9 blog-body">
			
			<?php if (isset($_SESSION['loginForCreate'])){ ?>
			<div class="alert alert-danger" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				Please, login for post creating
			</div>
			<?php } ?>
        
            <div class="col-lg-6 col-lg-offset-3 ng-scope">
				
                <div class="panel panel-success" style="margin-top:20px;">
				
                    <div class="panel-heading">
                        <h2 style="margin:0;" class="ng-binding">Login</h2>
                    </div>
                    <div class="panel-body">
                        <?php
                        if (isset($error)){ ?>
                            <div class="alert alert-danger" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								Authorization error
							</div>
						<?php } ?>
                        
                        <form method="POST" ng-submit="loginForm.submit()" novalidate="" name="loginFrm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required">

                                <?php 
                                    if (isset($_SESSION['auth']) && $_SESSION['auth']){
                                        echo 'Login success';
                                    }
                                ?>
                    
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-email">User name</label>
                                    <input type="text" id="loginform-email" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" name="username" ng-model="loginForm.email" ng-required="true" required="required">
                                </div>
                    
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-password">Password</label>
                                    <input type="password" id="loginform-password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" name="password" ng-model="loginForm.password" ng-required="true" required="required">
                                </div>
                    
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me" name="rememberMe" ng-model="loginForm.rememberMe" class="ng-pristine ng-untouched ng-valid"> Keep me logged in
                                    </label>
                                </div>
                    
                                <div class="form-group">
                                    <input type="submit" id="login-button" class="btn btn-primary form-control" value="Login">
                                </div>

								<div class="form-group">
                                    <a href="https://oauth.vk.com/authorize?client_id=5063855&display=page&redirect_uri=http://slabel-blog.phpschool/vkoauth.php">
                                    <!--<a href="https://oauth.vk.com/authorize?client_id=5063855&scope=email&display=page&redirect_uri=http://slabel-blog.phpschool/vkoauth.php">-->
										<input type="button" id="login-button" class="btn btn-primary form-control" value="Login with VK">
									</a>
										
                                </div>
                
                            </form>
                
                    </div>
                </div>
            </div>
        </div>    
        
    </div>
</body>
</html>