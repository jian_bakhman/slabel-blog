    <?php require_once ('header.php'); ?>
    <div class="container-fluid">
        <div class="col-md-3">
			<?php require_once ('menu.php'); ?>
        </div>
        <div class="col-md-9 blog-body">
	<?php foreach ($posts as $post) { ?>

		<div class="post">
			<a href="post.php?id=<?php echo $post['id'] ?>">
				<h2 class="post-title"><?php echo $post["title"] ?></h2>
			</a>
			<h3 class="post-subtitle">
				<?php echo $post['summary'] ?>
			</h3>

			<p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a> on <?php echo $post['date']; ?>
				<a href="post.php?id=<?php echo $post['id'] ?>" class="btn btn-primary btn-sm pull-right">Read More</a>
			</p>
			<hr />
		</div>

	<?php } ?>
            
	<?php
		$size = $fileDb->postsCount();
		$pages = ceil($size/$pageSize);	// определяем кол-во страниц
	?>
	<ul class="pagination pull-right" boundary-links="true">
                <li class="<?php echo ($page == 1) ? 'ng-scope disabled' : '' ?>"><a href="index.php" class="ng-binding">First</a></li>
                <li class="<?php echo ($page == 1) ? 'ng-scope disabled' : '' ?>"><a href="index.php?page=<?php echo $page-1; ?>" class="ng-binding">Previous</a></li>
			<?php
			for ($i=$page-3; $i<$page+4; $i++){
				if ($i>=1 && $i<=$pages){ ?>
					<li class="<?php echo ($i == $page) ? 'active' : ''; ?>	">
						<a href="index.php?page=<?php echo $i; ?>" class="ng-binding"><?php echo $i; ?></a>
					</li>
			<?php }
			} ?>
                
                <li class="<?php echo ($page == $pages) ? 'ng-scope disabled' : '' ?>"><a href="index.php?page=<?php echo $page+1; ?>">Next</a></li>
                <li class="<?php echo ($page == $pages) ? 'ng-scope disabled' : '' ?>"><a href="index.php?page=<?php echo $pages; ?>" >Last</a></li>
	</ul>
            <?php
			?>
            
        </div>    
        
    </div>
</body>
</html>