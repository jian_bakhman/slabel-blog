	<?php require_once ('header.php'); ?>
	<div class="container-fluid">
        <div class="col-md-3">
			<?php require_once ('menu.php'); ?>
		</div>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                
                <h1>Create new blog post</h1>
                
                <form method="POST">
                    <div class="form-group">
                        <label>Post title</label>
                        <input type="text" class="form-control" name="title" />
                    </div>
                    
                    <div class="form-group">
                        <label>Post summary</label>
                        <textarea class="form-control" name="summary"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Post body</label>
                        <textarea rows="10" class="form-control" name="body"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="Create" />
                    </div>
                </form>
                
                
                <hr />
            </div>
            
        </div>    
        
    </div>
</body>
</html>