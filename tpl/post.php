	<?php require_once ('header.php'); ?>
    <div class="container-fluid">
        <div class="col-md-3">
            <?php require_once ('menu.php'); ?>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                <h2 class="post-title"><?php echo $post['title'] ?></h2>
                <h3 class="post-subtitle">
					<?php echo $post['body'] ?>
                </h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a> on <?php echo $post['date']; ?>
					<?php if($_SESSION['auth']){ ?>
					<a href="delete.php?id=<?php echo $post['id'] ?>" class="btn btn-primary btn-sm pull-right">Delete this post</a>
					<a href="modify.php?id=<?php echo $post['id'] ?>" class="btn btn-primary btn-md pull-right">Modify</a>
					<?php } ?>
                </p>
                
                <hr />
            </div>
            
        </div>    
    </div>
</body>
</html>