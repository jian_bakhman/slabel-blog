	<?php require_once ('header.php'); ?>
	<div class="container-fluid">
        <div class="col-md-3">
			<?php require_once ('menu.php'); ?>
		</div>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                
                <h1>Modify post</h1>
                
                <form method="POST">
                    <div class="form-group">
                        <label>Post title</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $post['title'] ?>"/>
                    </div>
                    
                    <div class="form-group">
                        <label>Post summary</label>
                        <textarea class="form-control" name="summary" ><?php echo $post['summary'] ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Post body</label>
                        <textarea rows="10" class="form-control" name="body"><?php echo $post['body']; ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" name="savepost" value="" />
                    </div>
					
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="Save change" />
                    </div>
                </form>
                
                
                <hr />
            </div>
            
        </div>    
        
    </div>
</body>
</html>