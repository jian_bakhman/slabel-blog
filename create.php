<?php
/* 
 * Create post
 * 
 */
$config = require_once ('config.php');
require_once ('class-db.php');

$fileDb = new FileDb('db/', 'r', $config['pageSize']);

$currentMode = 'create';		// переменная для подсветки текущего пункта меню (menu.php)
$curHeadTitle = 'Create post';	// переменная для вывода title в head блога (menu.php)

/* Проверка авторизации на сайте при попытка написать пост */
if (!isset($_SESSION['auth'])){
	$_SESSION['loginForCreate'] = TRUE;
	header('Location: login.php');
}
/* Сохранение поста в папку db */
if (isset($_POST['title']) && isset($_POST['summary']) && isset($_POST['body'])){
	$postId = $fileDb->createPost($_POST['title'], $_POST['summary'], $_POST['body']);
	header('location: post.php?id=' . $postId);		//переход к просмотру поста после его сохранения
}

require_once ('tpl/create.php');
