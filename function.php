<?php

/**
 *  Function for blog
 */

/** Function for cURL qvery  **/

function curl_query($url, $postfields){

	$ch = curl_init($url);
	curl_setopt_array($ch, [
		CURLOPT_RETURNTRANSFER	 => TRUE,
		CURLOPT_SSL_VERIFYPEER	 => FALSE,
		CURLOPT_SSL_VERIFYHOST	 => FALSE,
		CURLOPT_POST			 => TRUE,
		CURLOPT_POSTFIELDS		 => $postfields,
	]);
	$curle = curl_exec($ch);
	return $curle;
}