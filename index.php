<?php
/* 
 * slabel-blog
 * 
 */
$config = require_once ('config.php');
require_once ('class-db.php');

$fileDb = new FileDb('db/', 'r', $config['pageSize']);

$currentMode = 'index';		// переменная для подсветки текущего пункта меню (menu.php)
$filename = 'db/post.json';

$page = 1;
$pageSize = $config['pageSize'];
	
if(isset($_GET['page']) && is_numeric($page) && $page >= 1) {
	$page = $_GET['page'];
}

$posts = $fileDb->getPosts($page);

require_once ('tpl/index.php');