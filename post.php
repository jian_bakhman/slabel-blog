<?php
/* 
 *  post.php in main blog
 */
$config = require_once ('config.php');

$id  = $_GET['id'];
$post = json_decode(file_get_contents("db/$id.json"), TRUE);	//получаем и декодируем пост из файлас названем $id

$curHeadTitle = $post['title'];		// переменная для вывода title в head блога (menu.php)
require_once ('tpl/post.php');