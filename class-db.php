<?php

/* 
 * Class constructor
 *
 * передается путь к файлу и режим записи
 */

abstract class Db {
	abstract function createPost($title, $summary, $body);
	abstract function updatePost($postId, $title, $summary, $body);
	abstract function getPosts($page);
	abstract function postsCount();
	abstract function deletePost($postId);
}

/*
class FileDB{
	private $filePath;
	private $fileLink = false;
	public function __construct($filepath) {
		$this->filePath = $filepath;
		if (file_exists($this->filePath)){
			$this->fileLink = fopen($filepath, NULL);
		}
	}
	public function writeFile($sting){
		if ($this->fileLink && !empty($sting)){
			fwrite($this->fileLink, $string);
		}
	}
	public function writeJSON($array) {
		$json = json_encode($array);
		$this->writeFile($json . "\n");
	}
}
*/

class FileDb extends Db {

	private $path;
	private $filename;
	private $db;
	private $mode;
	private $pageSize;

	public function __construct($path, $mode, $pageSize) {
		$this->path = $path;
		$this->filename = 'post.json';
		$this->db = $this->path . $this->filename;
		$this->mode = $mode;
		$this->pageSize = $pageSize;
	}

	public function createPost($title, $summary, $body) {
		$newPostSummary = array();
		$newPostBody = array();
		$newPostSummary['id'] = $newPostBody['id'] = sha1(time() . mt_rand(0, 10000));
		$newPostSummary['title'] = $newPostBody['title'] = $title;
		$newPostSummary['summary'] = $newPostBody['summary'] = $summary;
		$newPostSummary['date'] = $newPostBody['date'] = date('d.m.Y H:i:s');
		$newPostBody['body'] = $body;
		$postId = $newPostBody["id"];

		file_put_contents($this->db, "\n" . json_encode($newPostSummary), FILE_APPEND);
		file_put_contents("db/$postId.json", json_encode($newPostBody));

		return $postId;
	}

	public function updatePost($postId, $title, $summary, $body){
		$filename = $postId . '.json';
		$newPostSummary = array();
		$newPostBody = array();
		$newPostSummary['id'] = $newPostBody['id'] = $postId;
		$newPostSummary['title'] = $newPostBody['title'] = $title;
		$newPostSummary['summary'] = $newPostBody['summary'] = $summary;
		$newPostSummary['date'] = $newPostBody['date'] = date('d.m.Y H:i:s');
		$newPostBody['body'] = $body;

		$posts = file($this->db);	// загружаем в массив все посты построчно

		foreach ($posts as $key => $value) {
			if (strpos($value, $postId)) {
				$post = json_decode($value, TRUE);
				if ($post['id'] = $postId){
					$posts[$key] = json_encode($newPostSummary) . "\n";
					file_put_contents($this->db, $posts);
					break;
				}
			}
		}

		file_put_contents($this->path . $filename, json_encode($newPostBody));

	}

	public function getPosts($page){
		$posts = [];
		if (file_exists($this->db)) {
			$postsFile = fopen($this->db, $this->mode);
			
			$offset = ($page - 1) * $this->pageSize;
			$end = $offset + $this->pageSize;

			for ($i = 0; $i < $offset; $i++) {
				fgets($postsFile);
			}

			for ($i = $offset; ($i < $end && !feof($postsFile)); $i++) {
				$posts[] = json_decode(fgets($postsFile), TRUE);
			}
		}
		return $posts;
	}

	public function postsCount(){
		$count = file($this->db);		// загружаем в массив все посты построчно
		$postsCount = count($count);	// определяем кол-во постов
		return $postsCount;
	}

	public function deletePost($postId){
		$postsFile = fopen($this->db, 'r');
		$post = array();
		while (!feof($postsFile)){
			$post = json_decode(fgets($postsFile),TRUE);
			if ($post['id'] != $postId){
				$posts = $posts . "\n" . json_encode($post, TRUE);  // образуется лишний ведущий "\n"
			}
		}
		fclose($postsFile);

		$posts = trim($posts); // удаление ведущего "\n"
		file_put_contents($this->db, $posts);
		unlink("$this->path/$postId.json");
	}

	/* метод __get возвращает посты за последний месяц, неделю или год
	 * (например $fdb->week, $fdb->month, $fdb->year должны возвращать массивы с постами) */
	public function __get($name) {

	}

	/*  метод __toString() возвращает количество постов в блоге */
	public function __toString() {

	}

}